#!/usr/bin/env python
import geom2D
import geom3D
import time
import BoundaryFix as bf

start = time.clock()

print "================= Welcome to GeoFEMCompute ==========================="
print "Please enter the input tetgen file name. Example:    cube.ele ------> enter cube "
print "Then enter the name which supplies boundary information to correct wrong boundary markers"

filename = raw_input('Enter the tetgen file name (without suffix) to process: ') or 'default_file.asm'
polyname = raw_input('Supply the boundary file name (without suffix) to process: ') or 'default_file.asm'

path = '/home/jiguang/Desktop/Mixed_Method/Geometry/' + filename
polypath = '/home/jiguang/Desktop/Mixed_Method/Geometry/' + polyname + '.boundary'

geom3D.mesh_handling(path,polypath, filename)

elapsed = (time.clock() - start)

print "Total CPU time is", elapsed

