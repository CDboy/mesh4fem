#!/usr/bin/env python
import geom2D
import time

start = time.clock()

print "================= Welcome to GeoFEMCompute ==========================="
print "Please enter the input triangle file name. Example:    cube.ele ------> enter cube "

filename = raw_input('Enter the triangle file name (without suffix) to process: ') or 'default_file.asm'

path = '/home/jiguang/Desktop/Mixed_Method/Geometry/' + filename

geom2D.mesh_handling(path)

elapsed = (time.clock() - start)

print "Total CPU time is", elapsed