'''

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

Author: Jiguang Shen
Date  : June 11 2016

==== functionality ====
Read the mesh from tetgen mesh generator

Compute necessary geometrical information

Generate output files in ASCII format:
node3D.dat
node_quad3D.dat
element3D.dat
element_quad3D.dat
face3D.dat
DBCnode3D.dat (CG)
DBCface3D.dat (HDG)
PBCnode3D.dat (CG)
NBCface3D.dat (HDG)
RBCface3D.dat (HDG)
PBCface3D.dat (HDG)
'''

import numpy as np
import math

# determine if a point is inside a given polygon or not
# Polygon is a list of (x,y,z) pairs.

def point_inside_polygon(x,y,z,poly):
    '''
    The algorithm is due to Reinier van Vliet and Remco Lam
    Sum up all angles between line from q to all vertices and all edges
    If the sum is 2 pi and all angle are non zero then we have it inside
    If the sum is 2 pi but two angels are very close to zero


    :param x:
    :param y:
    :param z:
    :param poly:
    :return: inside, onedge, onnode
    '''
    n = len(poly)
    inside = False
    onedge = False
    onnode = False
    angleSum = 0.0

    p1x, p1y, p1z, marker = poly[0]

    for i in range(1,n+1):
        p2x, p2y, p2z, marker = poly[i % n]
        p1 = np.array([p1x - x, p1y - y, p1z - z])
        p2 = np.array([p2x - x, p2y - y, p2z - z])

        m1 = np.linalg.norm(p1)
        m2 = np.linalg.norm(p2)

        cosine = 0.0

        if m1*m2 <= 10e-8:
           onnode = True
        else:
           cosine = (p1[0]*p2[0] + p1[1]*p2[1] + p1[2]*p2[2])/(m1*m2)

        p1x, p1y, p1z = p2x, p2y, p2z
        angleSum += math.acos(cosine)

        if abs(math.acos(cosine) - math.pi) < 10e-8:
           onedge = True


    if abs(angleSum - math.pi*2) < 10e-8:
       inside = True

    if inside & onedge:
       inside = False

    return inside,onedge,onnode


def boundary_marker_check(x,y,z,poly_list,marker):

    f2fEdge = [] #tracking edge nodes marker

    #check if on edge or in face or at node
    for i in range(0,len(poly_list)):
        inside,onedge,onnode = point_inside_polygon(x,y,z,poly_list[i])

        if onnode: #if on any of node, no need to continue, just break
           break
        if inside: #if inside on of the face, no need to continue, just fix the marker
           if marker != poly_list[i][0][3]:
              print "This point ", "(",x,y,z,")", " is inside facet. The previous marker", marker,"is changed to", poly_list[i][0][3]
              marker = poly_list[i][0][3]
           break
        if onedge: #tricker case, needs to look at neighbors
           f2fEdge.append(poly_list[i][0][3])

    if f2fEdge:
       if (f2fEdge[0] == f2fEdge[1])&(marker != f2fEdge[0]): #if shared by the two same type faces
          print "This point ", "(",x,y,z,")"," is on shared edge. The previous marker", marker, "is changed to", f2fEdge[0]
          marker = f2fEdge[0]

       if (f2fEdge[0] != f2fEdge[1]): # if shared by two different type faces
          if ((f2fEdge[0] == 1) | (f2fEdge[1] == 1))&(marker != 1): # one of it is DBC
             print "This point ", "(",x,y,z,")", " is on shared edge. The previous marker", marker, "is changed to", 1
             marker = 1

       if (f2fEdge[0] != f2fEdge[1]):  # if shared by two different type faces
          if ((f2fEdge[0] == 4) | (f2fEdge[1] == 4)) & (marker != 4) & ((f2fEdge[0] != 1) & (f2fEdge[1] != 1)):  # one of it is PBC, no DBC
             print "This point ", "(", x, y, z, ")", " is on shared edge. The previous marker", marker, "is changed to", 4
             marker = 4

    return marker

def mesh_handling(path, polyPath, outName):

    # construct path to different files:
    nodePath = path + ".node"
    elemPath = path + ".ele"
    facePath = path + ".face"
    nodeQuadPath = path + ".quad.node"
    elemQuadPath = path + ".quad.ele"

    print "Loading the polygonal information"
    pp_list = []
    poly_list = []

    with open(polyPath) as ply:
        first = next(ply).decode().split()
        num_pt = int(first[0])
        counter = 0
        for line_terminated in ply:
            counter += 1
            if counter <= num_pt:
                id, x, y, z = line_terminated.strip('\n').split()
                result = tuple([float(x), float(y), float(z)])
                pp_list.append(result)
            else:
                vr = line_terminated.strip('\n').split()
                vr = map(int, vr)
                vr = [tuple(list(pp_list[vr[i] - 1]) + [vr[len(vr) - 1]]) for i in range(0, len(vr) - 1)]
                poly_list.append(vr)


    print "Start reading node files"
    # read .node file
    DBC_node_list = []
    DBCquad_node_list = []
    PBC_node_list = []
    PBCquad_node_list = []

    with open(nodePath) as nodefile:
        first      = next(nodefile).decode().split()
        num_node   = int(first[0])              # the first line first position stores the number of node
        inode      = 1

        node_list  = []
        for line_terminated in nodefile:
            if inode == num_node + 1:
               break
            else:
                inode += 1
                id,x,y,z,marker = line_terminated.strip('\n').split()

                #check if marker is right
                new_marker = boundary_marker_check(float(x),float(y),float(z),poly_list,int(marker))
                node_list.append([id,x,y,z,new_marker])

                if int(new_marker) == 1:  # if Dirichlet node
                    DBC_node_list.append(int(id))
                if int(new_marker) == 4:  # if Periodic node
                    PBC_node_list.append(int(id))


    with open(outName+'_node3D.dat','wb') as nodefileout:
        for i in range(0,len(node_list)):
            str_line = str(node_list[i][0]) + ' ' + str(node_list[i][1]) + ' ' + str(node_list[i][2]) + ' ' + str(node_list[i][3]) + ' ' + str(node_list[i][4])

            if i != len(node_list):
                str_line = str_line + '\n'

            nodefileout.write(str_line)

    if DBC_node_list:
        with open(outName+'_DBCnode3D.dat', 'wb') as DBCout:
            for i in range(0, len(DBC_node_list)):
                 str_line = str(DBC_node_list[i]) + '\n'
                 DBCout.write(str_line)

    if PBC_node_list:
        with open(outName+'_PBCnode3D.dat', 'wb') as PBCout:
            for i in range(0, len(PBC_node_list)):
                 str_line = str(PBC_node_list[i]) + '\n'
                 PBCout.write(str_line)

    with open(nodeQuadPath) as nodequadfile:
        first      = next(nodequadfile).decode().split()
        num_node   = int(first[0])              # the first line first position stores the number of node
        inode      = 1

        node_quad_list  = []
        for line_terminated in nodequadfile:
            if inode == num_node + 1:
               break
            else:
                inode += 1
                id,x,y,z,marker = line_terminated.strip('\n').split()
                new_marker = boundary_marker_check(float(x),float(y),float(z),poly_list,int(marker))
                node_quad_list.append([id,x,y,z,new_marker])

                if int(new_marker) == 1:  # if Dirichlet node
                    DBCquad_node_list.append(int(id))
                if int(new_marker) == 4:  # if Periodic node
                    PBCquad_node_list.append(int(id))

    if DBCquad_node_list:
        with open(outName+'_DBCquadnode3D.dat', 'wb') as DBCout:
            for i in range(0, len(DBCquad_node_list)):
                str_line = str(DBCquad_node_list[i]) + '\n'
                DBCout.write(str_line)

    if PBCquad_node_list:
        with open(outName+'_PBCquadnode3D.dat', 'wb') as PBCout:
            for i in range(0, len(PBCquad_node_list)):
                str_line = str(PBCquad_node_list[i]) + '\n'
                PBCout.write(str_line)

    with open(outName + '_node3D.quad.dat','wb') as nodequadfileout:
        for i in range(0,len(node_quad_list)):
            str_line = str(node_quad_list[i][0]) + ' ' + str(node_quad_list[i][1]) + ' ' + str(node_quad_list[i][2]) + ' ' + str(node_quad_list[i][3]) + ' ' + str(node_quad_list[i][4])

            if i != len(node_quad_list):
                str_line = str_line + '\n'

            nodequadfileout.write(str_line)

    print "Node file processing done"



    print "Starting face file processing"
    # read .face file
    with open(facePath) as facefile:
        first      = next(facefile).decode().split()
        num_face   = int(first[0])              # the first line first position stores the number of edges
        iface      = 1

        face_list  = []
        DBC_face_list = []
        NBC_face_list = []
        RBC_face_list = []
        PBC_face_list = []


        for line_terminated in facefile:
            if iface == num_face + 1:
                break
            else:
                iface += 1
                id,v1,v2,v3,marker = line_terminated.strip('\n').split()

                '''
                   face information computation
                '''

                #compute face area
                p1 = np.array([float(node_list[int(v1) - 1][1]), float(node_list[int(v1) - 1][2]), float(node_list[int(v1) - 1][3])])
                p2 = np.array([float(node_list[int(v2) - 1][1]), float(node_list[int(v2) - 1][2]), float(node_list[int(v2) - 1][3])])
                p3 = np.array([float(node_list[int(v3) - 1][1]), float(node_list[int(v3) - 1][2]), float(node_list[int(v3) - 1][3])])

                vec1 = p1-p3
                vec2 = p1-p2

                area = 0.5*np.linalg.norm(np.cross(vec1,vec2))


                #compute face orientation
                '''
                We require face vertices are stored in ascending order
                '''

                v1 = int(v1)
                v2 = int(v2)
                v3 = int(v3)

                v1,v2,v3 = sorted([v1,v2,v3])

                face_list.append([id,v1,v2,v3,marker,area])

                if int(marker) == 1:
                   DBC_face_list.append([int(id),0,0])
                if int(marker) == 2:
                   NBC_face_list.append([int(id),0,0,0,0,0,0,0,0,0,0,0,0])
                if int(marker) == 3:
                   RBC_face_list.append([int(id),0,0,0,0,0,0,0,0,0,0,0,0])
                if int(marker) == 4:
                   PBC_face_list.append([int(id),0,0])

    list1 = [1,2,3]
    vert_list = [(int(each_list[1]),int(each_list[2]), int(each_list[3])) for each_list in face_list]



    with open(outName + '_face3D.dat','wb') as facefileout:
        for i in range(0,len(face_list)):
            str_line = str(face_list[i][0]) + ' ' + str(face_list[i][1]) + ' ' + str(face_list[i][2]) +\
                       ' ' + str(face_list[i][3]) + ' ' + str(face_list[i][4]) + ' ' + str(face_list[i][5])

            if i != len(face_list):
                str_line = str_line + '\n'

            facefileout.write(str_line)

    print "Face file handling done"

    print "Start reading element files"
    with open(elemQuadPath) as elequadfile:
        first = next(elequadfile).decode().split()
        num_ele = int(first[0])  # the first line first position stores the number of edges
        iele = 1

        elequad_list = []
        for line_terminated in elequadfile:
            if iele == num_ele + 1:
               break
            else:
                iele += 1
                id, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10 = line_terminated.strip('\n').split()

            elequad_list.append([id, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10])

    with open(outName + '_elem3D.quad.dat','wb') as elequadfile:
        for i in range(0,len(elequad_list)):
            str_line = str(elequad_list[i][0]) + ' ' + str(elequad_list[i][1]) + ' ' + str(elequad_list[i][2]) +\
                       ' ' + str(elequad_list[i][3]) + ' ' + str(elequad_list[i][4]) + ' ' + str(elequad_list[i][5]) + \
                       ' ' + str(elequad_list[i][6]) + ' ' + str(elequad_list[i][7]) + ' ' + str(elequad_list[i][8]) + \
                       ' ' + str(elequad_list[i][9]) + ' ' + str(elequad_list[i][10])


            if i != len(elequad_list):
                str_line = str_line + '\n'

            elequadfile.write(str_line)

    with open(elemPath) as elefile:
        first = next(elefile).decode().split()
        num_ele = int(first[0])  # the first line first position stores the number of edges
        iele = 1

        ele_list = []
        Jac = []
        for line_terminated in elefile:
            if iele == num_ele + 1:
               break
            else:
                iele += 1
                id, v1, v2, v3, v4 = line_terminated.strip('\n').split()

            #Element information computation

            #compute vol
            p1 = np.array([float(node_list[int(v1) - 1][1]), float(node_list[int(v1) - 1][2]), float(node_list[int(v1) - 1][3])])
            p2 = np.array([float(node_list[int(v2) - 1][1]), float(node_list[int(v2) - 1][2]), float(node_list[int(v2) - 1][3])])
            p3 = np.array([float(node_list[int(v3) - 1][1]), float(node_list[int(v3) - 1][2]), float(node_list[int(v3) - 1][3])])
            p4 = np.array([float(node_list[int(v4) - 1][1]), float(node_list[int(v4) - 1][2]), float(node_list[int(v4) - 1][3])])



            dx = p1-p4
            dy = p2-p4
            dz = p3-p4


            vol = 1.0/6.0 * abs(np.dot(dx, np.cross(dy,dz)))

            DepMat = np.matrix([[p1[0],p2[0],p3[0],p4[0]],[p1[1],p2[1],p3[1],p4[1]],[p1[2],p2[2],p3[2],p4[2]],[1.0,1.0,1.0,1.0]])

            DesMat = np.matrix([[0.0,1.0,0.0,0.0],[0.0,0.0,1.0,0.0],[0.0,0.0,0.0,1.0],[1.0,1.0,1.0,1.0]])

            Tmat = DesMat*DepMat.I

            Jac.append([1,0.0,0.0,0.0,0.0,Tmat[0,0],Tmat[1,0],Tmat[2,0],0.0,Tmat[0,1],Tmat[1,1],Tmat[2,1],0.0,Tmat[0,2],Tmat[1,2],Tmat[2,2]])
            '''
            We require the following local numbering


            face #1:  1 2 3
            face #2:  1 2 4
            face #3:  1 3 4
            face #4:  4 2 3

            The above order ensures normals to follow right hand rule

            '''

            '''
            Face #1 normal compute
            right hand rule:
            1-3 is the first vector
            1-2 is the second vector
            '''

            vc1 = p3-p1
            vc2 = p2-p1

            normal1 = np.cross(vc1,vc2)

            #normalization
            normal1 = normal1/np.linalg.norm(normal1)


            '''
            Face #2 normal compute
            right hand rule:
            1-4 is the first vector
            1-2 is the second vector
            '''

            vc1 = p4-p1
            vc2 = p2-p1

            normal2 = np.cross(vc1,vc2)

            #normalization
            normal2 = normal2/np.linalg.norm(normal2)


            '''
            Face #3 normal compute
            right hand rule:
            1-3 is the first vector
            1-4 is the second vector
            '''

            vc1 = p3-p1
            vc2 = p4-p1

            normal3 = np.cross(vc1,vc2)

            #normalization
            normal3 = normal3/np.linalg.norm(normal3)


            '''
            Face #4 normal compute
            right hand rule:
            2-3 is the first vector
            2-4 is the second vector
            '''

            vc1 = p3-p2
            vc2 = p4-p2

            normal4 = np.cross(vc1,vc2)

            #normalization
            normal4 = normal4/np.linalg.norm(normal4)


            #orientations predefine
            ori1 = 1
            ori2 = 1
            ori3 = 1
            ori4 = 1

            '''
            Orientation computation
            Most difficult part of the program

            We assume the following 6 orientation patterns

            ori  permutations

            1:   1   2   3
            2:   1   3   2
            3:   3   1   2
            4:   3   2   1
            5:   2   3   1
            6:   2   1   3

            What we do:

            Reference is defined as

              v3
              *
              * *
              *   *
              *     *
              *       *
              * * * * * *
              v1         v2


            All the triangle is mapped back to reference by the following rules:

            mapping the smallest index to v1, second smallest to v2, largest to v3

            This rule will generate a permutation of original index


            '''

            #================ face #1 ================
            n1 = int(v1)
            n2 = int(v2)
            n3 = int(v3)

            if (n1 < n2) & (n2 < n3): # n1 < n2 < n3
               ori1 = 1                # no change
            if (n1 < n3) & (n3 < n2): # n1 < n3 < n2
               ori1 = 2                # switch v2 and v3
            if (n2 < n3) & (n3 < n1): # n3 < n1 < n2
               ori1 = 3                # switch v1 and v3
            if (n3 < n2) & (n2 < n1): # n3 < n2 < n1
               ori1 = 4                # rotation by 1 cw
            if (n3 < n1) & (n1 < n2): # n2 < n3 < n1
               ori1 = 5                # rotation by 1 ccw
            if (n2 < n1) & (n1 < n3): # n2 < n1 < n3
               ori1 = 6                # switch v1 and v3

            vl = sorted([n1,n2,n3])
            vl = tuple(vl)

            f1 = vert_list.index(vl)+1

            if (int(face_list[f1 - 1][4]) == 1):  # if DBC
                nindx = DBC_face_list.index([f1,0,0])
                DBC_face_list[nindx][1] = id
                DBC_face_list[nindx][2] = 1


            if (int(face_list[f1 - 1][4]) == 4):  # if PBC
                nindx = PBC_face_list.index([f1,0,0])
                PBC_face_list[nindx][1] = id
                PBC_face_list[nindx][2] = 1

            if (int(face_list[f1 - 1][4]) == 2): # if NBC
                nindx = NBC_face_list.index([f1,0,0,0,0,0,0,0,0,0,0,0,0])
                NBC_face_list[nindx][1] = id
                NBC_face_list[nindx][2] = 1

                NBC_face_list[nindx][3] = node_list[int(v1)-1][1]
                NBC_face_list[nindx][4] = node_list[int(v1)-1][2]
                NBC_face_list[nindx][5] = node_list[int(v1)-1][3]

                NBC_face_list[nindx][6] = node_list[int(v3)-1][1]
                NBC_face_list[nindx][7] = node_list[int(v3)-1][2]
                NBC_face_list[nindx][8] = node_list[int(v3)-1][3]

                NBC_face_list[nindx][9]  = node_list[int(v2)-1][1]
                NBC_face_list[nindx][10] = node_list[int(v2)-1][2]
                NBC_face_list[nindx][11] = node_list[int(v2)-1][3]

                NBC_face_list[nindx][12] = face_list[f1 - 1][5]


            if (int(face_list[f1 - 1][4]) == 3): # if RBC
                rindx = RBC_face_list.index([f1,0,0,0,0,0,0,0,0,0,0,0,0])
                RBC_face_list[rindx][1] = id
                RBC_face_list[rindx][2] = 1

                RBC_face_list[rindx][3] = node_list[int(v1)-1][1]
                RBC_face_list[rindx][4] = node_list[int(v1)-1][2]
                RBC_face_list[rindx][5] = node_list[int(v1)-1][3]

                RBC_face_list[rindx][6] = node_list[int(v3)-1][1]
                RBC_face_list[rindx][7] = node_list[int(v3)-1][2]
                RBC_face_list[rindx][8] = node_list[int(v3)-1][3]

                RBC_face_list[rindx][9]  = node_list[int(v2)-1][1]
                RBC_face_list[rindx][10] = node_list[int(v2)-1][2]
                RBC_face_list[rindx][11] = node_list[int(v2)-1][3]

                RBC_face_list[rindx][12] = face_list[f1 - 1][5]

            #================ face #2 ================
            n1 = int(v1)
            n2 = int(v2)
            n3 = int(v4)

            if (n1 < n2) & (n2 < n3):  # n1 < n2 < n3
                ori1 = 1  # no change
            if (n1 < n3) & (n3 < n2):  # n1 < n3 < n2
                ori1 = 2  # switch v2 and v3
            if (n2 < n3) & (n3 < n1):  # n3 < n1 < n2
                ori1 = 3  # switch v1 and v3
            if (n3 < n2) & (n2 < n1):  # n3 < n2 < n1
                ori1 = 4  # rotation by 1 cw
            if (n3 < n1) & (n1 < n2):  # n2 < n3 < n1
                ori1 = 5  # rotation by 1 ccw
            if (n2 < n1) & (n1 < n3):  # n2 < n1 < n3
                ori1 = 6  # switch v1 and v3

            vl = sorted([n1, n2, n3])
            vl = tuple(vl)

            f2 = vert_list.index(vl)+1
            if (int(face_list[f2 - 1][4]) == 1):  # if DBC
                nindx = DBC_face_list.index([f2,0,0])
                DBC_face_list[nindx][1] = id
                DBC_face_list[nindx][2] = 2

            if (int(face_list[f2 - 1][4]) == 4):  # if PBC
                nindx = PBC_face_list.index([f2,0,0])
                PBC_face_list[nindx][1] = id
                PBC_face_list[nindx][2] = 2

            if (int(face_list[f2 - 1][4]) == 2):  # if NBC
                nindx = NBC_face_list.index([f2, 0, 0,0,0,0,0,0,0,0,0,0,0])
                NBC_face_list[nindx][1] = id
                NBC_face_list[nindx][2] = 2

                NBC_face_list[nindx][3] = node_list[int(v1)-1][1]
                NBC_face_list[nindx][4] = node_list[int(v1)-1][2]
                NBC_face_list[nindx][5] = node_list[int(v1)-1][3]

                NBC_face_list[nindx][6] = node_list[int(v4)-1][1]
                NBC_face_list[nindx][7] = node_list[int(v4)-1][2]
                NBC_face_list[nindx][8] = node_list[int(v4)-1][3]

                NBC_face_list[nindx][9]  = node_list[int(v3)-1][1]
                NBC_face_list[nindx][10] = node_list[int(v3)-1][2]
                NBC_face_list[nindx][11] = node_list[int(v3)-1][3]

                NBC_face_list[nindx][12] = face_list[f2-1][5]


            if (int(face_list[f2 - 1][4]) == 3):  # if RBC
                rindx = RBC_face_list.index([f2, 0, 0,0,0,0,0,0,0,0,0,0,0])
                RBC_face_list[rindx][1] = id
                RBC_face_list[rindx][2] = 2


                RBC_face_list[rindx][3] = node_list[int(v1)-1][1]
                RBC_face_list[rindx][4] = node_list[int(v1)-1][2]
                RBC_face_list[rindx][5] = node_list[int(v1)-1][3]

                RBC_face_list[rindx][6] = node_list[int(v4)-1][1]
                RBC_face_list[rindx][7] = node_list[int(v4)-1][2]
                RBC_face_list[rindx][8] = node_list[int(v4)-1][3]

                RBC_face_list[rindx][9]  = node_list[int(v3)-1][1]
                RBC_face_list[rindx][10] = node_list[int(v3)-1][2]
                RBC_face_list[rindx][11] = node_list[int(v3)-1][3]

                RBC_face_list[rindx][12] = face_list[f2-1][5]


           #================ face #3 ================
            n1 = int(v1)
            n2 = int(v3)
            n3 = int(v4)

            if (n1 < n2) & (n2 < n3):  # n1 < n2 < n3
                ori1 = 1  # no change
            if (n1 < n3) & (n3 < n2):  # n1 < n3 < n2
                ori1 = 2  # switch v2 and v3
            if (n2 < n3) & (n3 < n1):  # n3 < n1 < n2
                ori1 = 3  # switch v1 and v3
            if (n3 < n2) & (n2 < n1):  # n3 < n2 < n1
                ori1 = 4  # rotation by 1 cw
            if (n3 < n1) & (n1 < n2):  # n2 < n3 < n1
                ori1 = 5  # rotation by 1 ccw
            if (n2 < n1) & (n1 < n3):  # n2 < n1 < n3
                ori1 = 6  # switch v1 and v3

            vl = sorted([n1, n2, n3])
            vl = tuple(vl)

            f3 = vert_list.index(vl)+1
            if (int(face_list[f3 - 1][4]) == 1):  # if DBC
                nindx = DBC_face_list.index([f3,0,0])
                DBC_face_list[nindx][1] = id
                DBC_face_list[nindx][2] = 3

            if (int(face_list[f3 - 1][4]) == 4):  # if PBC
                nindx = PBC_face_list.index([f3,0,0])
                PBC_face_list[nindx][1] = id
                PBC_face_list[nindx][2] = 3

            if (int(face_list[f3 - 1][4]) == 2):  # if NBC
                nindx = NBC_face_list.index([f3, 0, 0,0,0,0,0,0,0,0,0,0,0])
                NBC_face_list[nindx][1] = id
                NBC_face_list[nindx][2] = 3

                NBC_face_list[nindx][3] = node_list[int(v1)-1][1]
                NBC_face_list[nindx][4] = node_list[int(v1)-1][2]
                NBC_face_list[nindx][5] = node_list[int(v1)-1][3]

                NBC_face_list[nindx][6] = node_list[int(v2)-1][1]
                NBC_face_list[nindx][7] = node_list[int(v2)-1][2]
                NBC_face_list[nindx][8] = node_list[int(v2)-1][3]

                NBC_face_list[nindx][9] = node_list[int(v4)-1][1]
                NBC_face_list[nindx][10] = node_list[int(v4)-1][2]
                NBC_face_list[nindx][11] = node_list[int(v4)-1][3]

                NBC_face_list[nindx][12] = face_list[f3-1][5]


            if (int(face_list[f3 - 1][4]) == 3):  # if RBC
                rindx = RBC_face_list.index([f3, 0, 0,0,0,0,0,0,0,0,0,0,0])
                RBC_face_list[rindx][1] = id
                RBC_face_list[rindx][2] = 3

                RBC_face_list[rindx][3] = node_list[int(v1)-1][1]
                RBC_face_list[rindx][4] = node_list[int(v1)-1][2]
                RBC_face_list[rindx][5] = node_list[int(v1)-1][3]

                RBC_face_list[rindx][6] = node_list[int(v2)-1][1]
                RBC_face_list[rindx][7] = node_list[int(v2)-1][2]
                RBC_face_list[rindx][8] = node_list[int(v2)-1][3]

                RBC_face_list[rindx][9]  = node_list[int(v4)-1][1]
                RBC_face_list[rindx][10] = node_list[int(v4)-1][2]
                RBC_face_list[rindx][11] = node_list[int(v4)-1][3]

                RBC_face_list[rindx][12] = face_list[f3 - 1][5]

            #================ face #4 ================
            n1 = int(v4)
            n2 = int(v2)
            n3 = int(v3)

            if (n1 < n2) & (n2 < n3):  # n1 < n2 < n3
                ori1 = 1  # no change
            if (n1 < n3) & (n3 < n2):  # n1 < n3 < n2
                ori1 = 2  # switch v2 and v3
            if (n2 < n3) & (n3 < n1):  # n3 < n1 < n2
                ori1 = 3  # switch v1 and v3
            if (n3 < n2) & (n2 < n1):  # n3 < n2 < n1
                ori1 = 4  # rotation by 1 cw
            if (n3 < n1) & (n1 < n2):  # n2 < n3 < n1
                ori1 = 5  # rotation by 1 ccw
            if (n2 < n1) & (n1 < n3):  # n2 < n1 < n3
                ori1 = 6  # switch v1 and v3

            vl = sorted([n1, n2, n3])
            vl = tuple(vl)

            f4 = vert_list.index(vl)+1
            if (int(face_list[f4 - 1][4]) == 1):  # if DBC
                nindx = DBC_face_list.index([f4,0,0])
                DBC_face_list[nindx][1] = id
                DBC_face_list[nindx][2] = 4

            if (int(face_list[f4 - 1][4]) == 4):  # if PBC
                nindx = PBC_face_list.index([f4,0,0])
                PBC_face_list[nindx][1] = id
                PBC_face_list[nindx][2] = 4


            if (int(face_list[f4 - 1][4]) == 2):  # if NBC
                nindx = NBC_face_list.index([f4, 0, 0,0,0,0,0,0,0,0,0,0,0])
                NBC_face_list[nindx][1] = id
                NBC_face_list[nindx][2] = 4

                NBC_face_list[nindx][3] = node_list[int(v2)-1][1]
                NBC_face_list[nindx][4] = node_list[int(v2)-1][2]
                NBC_face_list[nindx][5] = node_list[int(v2)-1][3]

                NBC_face_list[nindx][6] = node_list[int(v3)-1][1]
                NBC_face_list[nindx][7] = node_list[int(v3)-1][2]
                NBC_face_list[nindx][8] = node_list[int(v3)-1][3]

                NBC_face_list[nindx][9] = node_list[int(v4)-1][1]
                NBC_face_list[nindx][10] = node_list[int(v4)-1][2]
                NBC_face_list[nindx][11] = node_list[int(v4)-1][3]

                NBC_face_list[nindx][12] = face_list[f4-1][5]


            if (int(face_list[f4 - 1][4]) == 3):  # if RBC
                rindx = RBC_face_list.index([f4, 0, 0,0,0,0,0,0,0,0,0,0,0])
                RBC_face_list[rindx][1] = id
                RBC_face_list[rindx][2] = 4

                RBC_face_list[rindx][3] = node_list[int(v2)-1][1]
                RBC_face_list[rindx][4] = node_list[int(v2)-1][2]
                RBC_face_list[rindx][5] = node_list[int(v2)-1][3]

                RBC_face_list[rindx][6] = node_list[int(v3)-1][1]
                RBC_face_list[rindx][7] = node_list[int(v3)-1][2]
                RBC_face_list[rindx][8] = node_list[int(v3)-1][3]

                RBC_face_list[rindx][9] = node_list[int(v4)-1][1]
                RBC_face_list[rindx][10] = node_list[int(v4)-1][2]
                RBC_face_list[rindx][11] = node_list[int(v4)-1][3]

                RBC_face_list[rindx][12] = face_list[f4-1][5]


            ele_list.append([id, v1, v2, v3, v4, vol, normal1[0], normal1[1], normal1[2],
                             normal2[0], normal2[1], normal2[2],normal3[0], normal3[1], normal3[2],
                             normal4[0], normal4[1], normal4[2],
                             f1, f2, f3, f4, ori1, ori2, ori3, ori4])

            if (iele == int(num_ele/4)):
                print "25% done..."
            if (iele == int(num_ele/2)):
                print "50% done..."
            if (iele == int(num_ele*3/4)):
                print "75% done..."




    with open(outName + '_elem3D.dat', 'wb') as elemfileout:
        for i in range(0, len(ele_list)):
            str_line = str(ele_list[i][0]) + ' ' + str(ele_list[i][1]) + ' ' + str(ele_list[i][2]) + \
                ' ' + str(ele_list[i][3]) + ' ' + str(ele_list[i][4]) + ' ' + str(ele_list[i][5]) + \
                ' ' + str(ele_list[i][6]) + ' ' + str(ele_list[i][7]) + ' ' + str(ele_list[i][8]) + \
                ' ' + str(ele_list[i][9]) + ' ' + str(ele_list[i][10]) + ' ' + str(ele_list[i][11]) + \
                ' ' + str(ele_list[i][12]) + ' ' + str(ele_list[i][13]) + ' ' + str(ele_list[i][14]) + \
                ' ' + str(ele_list[i][15]) + ' ' + str(ele_list[i][16]) + ' ' + str(ele_list[i][17]) + \
                ' ' + str(ele_list[i][18]) + ' ' + str(ele_list[i][19]) + ' ' + str(ele_list[i][20]) + \
                ' ' + str(ele_list[i][21]) + ' ' + str(ele_list[i][22]) + ' ' + str(ele_list[i][23]) + \
                ' ' + str(ele_list[i][24]) + ' ' + str(ele_list[i][25])
            if i != len(ele_list):
               str_line = str_line + '\n'

            elemfileout.write(str_line)

    if DBC_face_list:
        with open(outName + '_DBCface3D.dat', 'wb') as DBCout:
             for i in range(0, len(DBC_face_list)):
                 str_line = str(DBC_face_list[i][0]) + ' ' + str(DBC_face_list[i][1]) + '\n'
                 DBCout.write(str_line)


    if PBC_face_list:
        with open(outName +'_PBCface3D.dat', 'wb') as PBCout:
             for i in range(0, len(PBC_face_list)):
                 str_line = str(PBC_face_list[i][0]) + ' ' + str(PBC_face_list[i][1]) + '\n'
                 PBCout.write(str_line)
    if NBC_face_list:
        with open(outName + '_NBCface3D.dat', 'wb') as NBCout:
             for i in range(0, len(NBC_face_list)):
                 str_line = str(NBC_face_list[i][0]) + ' ' + str(NBC_face_list[i][1]) + ' ' + str(NBC_face_list[i][2])+ ' '\
                         + str(NBC_face_list[i][3]) + ' ' + str(NBC_face_list[i][4]) + ' ' + str(NBC_face_list[i][5])+ ' '\
                         + str(NBC_face_list[i][6]) + ' ' + str(NBC_face_list[i][7]) + ' ' + str(NBC_face_list[i][8])+ ' '\
                         + str(NBC_face_list[i][9]) + ' ' + str(NBC_face_list[i][10]) + ' ' + str(NBC_face_list[i][11]) + ' '\
                         + str(NBC_face_list[i][12]) + '\n'
                 NBCout.write(str_line)
    if RBC_face_list:
        with open(outName + '_RBCface3D.dat', 'wb') as RBCout:
             for i in range(0, len(RBC_face_list)):
                 str_line = str(RBC_face_list[i][0]) + ' ' + str(RBC_face_list[i][1]) + ' ' + str(RBC_face_list[i][2])+ ' '\
                         + str(RBC_face_list[i][3]) + ' ' + str(RBC_face_list[i][4]) + ' ' + str(RBC_face_list[i][5])+ ' '\
                         + str(RBC_face_list[i][6]) + ' ' + str(RBC_face_list[i][7]) + ' ' + str(RBC_face_list[i][8])+ ' '\
                         + str(RBC_face_list[i][9]) + ' ' + str(RBC_face_list[i][10]) + ' ' + str(RBC_face_list[i][11]) + ' '\
                         + str(RBC_face_list[i][12]) + '\n'
                 RBCout.write(str_line)


    with open(outName + '_JacobianEle3D.dat', 'wb') as Jacout:
        for i in range(0, len(Jac)):
            str_line = str(Jac[i][0]) + ' ' + str(Jac[i][1]) + ' ' + str(Jac[i][2]) + \
                       ' ' + str(Jac[i][3]) + ' ' + str(Jac[i][4]) + ' ' + str(Jac[i][5]) + \
                       ' ' + str(Jac[i][6]) + ' ' + str(Jac[i][7]) + ' ' + str(Jac[i][8]) + \
                       ' ' + str(Jac[i][9]) + ' ' + str(Jac[i][10]) + ' ' + str(Jac[i][11]) + \
                       ' ' + str(Jac[i][12]) + ' ' + str(Jac[i][13]) + ' ' + str(Jac[i][14]) + \
                       ' ' + str(Jac[i][15]) + '\n'

            Jacout.write(str_line)

    print "Element file handling done"
