'''
Author: Jiguang Shen
Date  : May 30 2016

=== functionality ===
Read the mesh from triangle mesh generator

Compute necessary geometrical information

Generate output files in ASCII format:
node.dat
elem.dat
edge.dat
DBCnode.dat (optional)
PBCnode.dat (optional)
non_DBC_edge.dat (optional)
JacobianEle.dat
'''

import numpy as np
import math

def mesh_handling(path):

    # construct path to different files:
    nodePath = path + ".node"
    elemPath = path + ".ele"
    edgePath = path + ".edge"

    print "Start reading node files"
    # read .node file
    with open(nodePath) as nodefile:
        first      = next(nodefile).decode().split()
        num_node   = int(first[0])              # the first line first position stores the number of node
        inode      = 1

        node_list  = []
        DBC_node_list = []
        PBC_node_list = []

        for line_terminated in nodefile:
            if inode == num_node + 1:
               break
            else:
                inode += 1
                id,x,y,marker = line_terminated.strip('\n').split()
                node_list.append([id,x,y,marker])
                if int(marker) == 1: #if Dirichlet node
                   DBC_node_list.append(int(id))
                if int(marker) == 4: #if Periodic node
                   PBC_node_list.append(int(id))

    with open('node.dat','wb') as nodefileout:
        for i in range(0,len(node_list)):
            str_line = str(node_list[i][0]) + ' ' + str(node_list[i][1]) + ' ' + str(node_list[i][2]) + ' ' + str(node_list[i][3])

            if i != len(node_list):
                str_line = str_line + '\n'

            nodefileout.write(str_line)

    if DBC_node_list:
        with open('DBCnode.dat', 'wb') as DBCout:
             for i in range(0, len(DBC_node_list)):
                 str_line = str(DBC_node_list[i]) + '\n'
                 DBCout.write(str_line)

    if PBC_node_list:
        with open('PBCnode.dat', 'wb') as PBCout:
            for i in range(0, len(PBC_node_list)):
                str_line = str(PBC_node_list[i]) + '\n'
                PBCout.write(str_line)

    print "Node file processing done"
    #filend = open('node.bin', 'wb')
    #n_list = np.asarray(node_list)
    #n_list.tofile(filend)

    print "Starting edge file processing"
    # read .edge file
    with open(edgePath) as edgfile:
        first      = next(edgfile).decode().split()
        num_edge   = int(first[0])              # the first line first position stores the number of edges
        iedg      = 1

        edge_list  = []
        for line_terminated in edgfile:
            if iedg == num_edge + 1:
               break
            else:
                iedg += 1
                id,v1,v2,marker = line_terminated.strip('\n').split()

                '''
                   Edge information computation
                '''
                length = math.sqrt((float(node_list[int(v1)-1][1]) - float(node_list[int(v2)-1][1]))**2  + \
                                   (float(node_list[int(v1)-1][2]) - float(node_list[int(v2)-1][2]))**2 )
                ori = 1
                if int(v1) > int(v2):
                   v3  = v1
                   v1  = v2
                   v2  = v3
                   ori = -1

                edge_list.append([id,v1,v2,marker,length, ori])

    list1 = [1,2]
    vert_list = [(int(each_list[1]),int(each_list[2])) for each_list in edge_list]
    #print vert_list.index((2,18))


    with open('edge.dat','wb') as edgefileout:
        for i in range(0,len(edge_list)):
            str_line = str(edge_list[i][0]) + ' ' + str(edge_list[i][1]) + ' ' + str(edge_list[i][2]) +\
                       ' ' + str(edge_list[i][3]) + ' ' + str(edge_list[i][4]) + ' ' + str(edge_list[i][5])

            if i != len(edge_list):
                str_line = str_line + '\n'

            edgefileout.write(str_line)

    print "Edge file handling done"

    #filedg = open('edge.bin', 'wb')
    #v_list = np.asarray(edge_list)
    #v_list.tofile(filedg)

    print "Start reading element files"
    Jac = []

    # read .edge file
    with open(elemPath) as elefile:
        first = next(elefile).decode().split()
        num_ele = int(first[0])  # the first line first position stores the number of edges
        iele = 1

        ele_list = []
        Non_DBC_edge_list = []
        for line_terminated in elefile:
            if iele == num_ele + 1:
               break
            else:
                iele += 1
                try:
                    id, v1, v2, v3, marker = line_terminated.strip('\n').split()
                except:
                    id, v1, v2, v3 = line_terminated.strip('\n').split()


            #Element information computation

            #compute area
            p1 = np.array([float(node_list[int(v1) - 1][1]), float(node_list[int(v1) - 1][2])])
            p2 = np.array([float(node_list[int(v2) - 1][1]), float(node_list[int(v2) - 1][2])])
            p3 = np.array([float(node_list[int(v3) - 1][1]), float(node_list[int(v3) - 1][2])])

            dy = (p2[0] - p1[0]) * (p3[1] - p1[1])
            dx = (p2[1] - p1[1]) * (p3[0] - p1[0])
            area = 0.5 * (dy - dx)

            Jac.append([1,0,0,0,(p3[1]-p1[1])/(dy-dx), (p1[1]-p2[1])/(dy-dx),0,(p1[0]-p3[0])/(dy-dx), (p2[0]-p1[0])/(dy-dx)])

            #compute normal
            nrm1_1 = p3[1]-p2[1]
            nrm1_2 = p2[0]-p3[0]
            nrm    = math.sqrt(nrm1_1**2 + nrm1_2**2)
            nrm1_1 = nrm1_1/nrm
            nrm1_2 = nrm1_2/nrm

            nrm2_1 = p1[1] - p3[1]
            nrm2_2 = p3[0] - p1[0]
            nrm    = math.sqrt(nrm2_1 ** 2 + nrm2_2 ** 2)
            nrm2_1 = nrm2_1 / nrm
            nrm2_2 = nrm2_2 / nrm

            nrm3_1 = p2[1] - p1[1]
            nrm3_2 = p1[0] - p2[0]
            nrm    = math.sqrt(nrm3_1 ** 2 + nrm3_2 ** 2)
            nrm3_1 = nrm3_1 / nrm
            nrm3_2 = nrm3_2 / nrm

            #orientations
            ori1 = 1
            ori2 = 1
            ori3 = 1

            #rotation matrix
            rot = np.array([[0,-1],[1,0]])


            #================ EDGE #1 ================
            n1 = int(v2)
            n2 = int(v3)

            if n1 > n2:
              aux  = n1
              n1   = n2
              n2   = aux
              ori1 = -1

            e1 = vert_list.index((n1, n2))+1

            if (int(edge_list[e1-1][3]) != 0) & (int(edge_list[e1-1][3]) != 1):
                nvec = np.array([nrm1_1,nrm1_2])
                trm = np.dot(rot,nvec)
                pt1 = int(edge_list[e1 - 1][1])
                pt2 = int(edge_list[e1 - 1][2])
                Non_DBC_edge_list.append([int(edge_list[e1-1][3]), int(id), 1, nrm1_1,nrm1_2, trm[0], trm[1],\
                                          node_list[pt1-1][1], node_list[pt1-1][2], node_list[pt2-1][1],\
                                          node_list[pt2-1][2], float(edge_list[e1-1][4])])

            # ================ EDGE #2 ================
            n1 = int(v3)
            n2 = int(v1)

            if n1 > n2:
             aux  = n1
             n1   = n2
             n2   = aux
             ori2 = -1

            e2 = vert_list.index((n1, n2))+1

            if (int(edge_list[e2 - 1][3]) != 0) & (int(edge_list[e2 - 1][3]) != 1):
                nvec = np.array([nrm2_1, nrm2_2])
                trm = np.dot(rot, nvec)
                pt1 = int(edge_list[e2 - 1][1])
                pt2 = int(edge_list[e2 - 1][2])
                Non_DBC_edge_list.append([int(edge_list[e2 - 1][3]), int(id), 2, nrm2_1, nrm2_2, trm[0], trm[1], \
                                          node_list[pt1 - 1][1], node_list[pt1 - 1][2], node_list[pt2 - 1][1], \
                                          node_list[pt2 - 1][2], float(edge_list[e2 - 1][4])])

           # ================ EDGE #3 ================
            n1 = int(v1)
            n2 = int(v2)

            if n1 > n2:
              aux  = n1
              n1   = n2
              n2   = aux
              ori3 = -1

            e3 = vert_list.index((n1, n2))+1

            if (int(edge_list[e3 - 1][3]) != 0) & (int(edge_list[e3 - 1][3]) != 1):
                nvec = np.array([nrm3_1, nrm3_2])
                trm = np.dot(rot, nvec)
                pt1 = int(edge_list[e3 - 1][1])
                pt2 = int(edge_list[e3 - 1][2])
                Non_DBC_edge_list.append([int(edge_list[e3 - 1][3]), int(id), 3, nrm3_1, nrm3_2, trm[0], trm[1], \
                                          node_list[pt1 - 1][1], node_list[pt1 - 1][2], node_list[pt2 - 1][1], \
                                          node_list[pt2 - 1][2], float(edge_list[e3 - 1][4])])


            ele_list.append([id, v1, v2, v3, area, nrm1_1, nrm1_2, nrm2_1, nrm2_2, nrm3_1, nrm3_2,\
                             e1, e2 , e3, ori1, ori2, ori3])

            if (iele == int(num_ele/4)):
                print "25% done..."
            if (iele == int(num_ele / 2)):
                print "50% done..."
            if (iele == int(num_ele*3 / 4)):
                print "75% done..."




    with open('elem.dat', 'wb') as elemfileout:
        for i in range(0, len(ele_list)):
                str_line = str(ele_list[i][0]) + ' ' + str(ele_list[i][1]) + ' ' + str(ele_list[i][2]) + \
                           ' ' + str(ele_list[i][3]) + ' ' + str(ele_list[i][4]) + ' ' + str(ele_list[i][5]) + \
                           ' ' + str(ele_list[i][6]) + ' ' + str(ele_list[i][7]) + ' ' + str(ele_list[i][8]) + \
                           ' ' + str(ele_list[i][9]) + ' ' + str(ele_list[i][10]) + ' ' + str(ele_list[i][11]) + \
                           ' ' + str(ele_list[i][12]) + ' ' + str(ele_list[i][13]) + ' ' + str(ele_list[i][14]) + \
                           ' ' + str(ele_list[i][15]) + ' ' + str(ele_list[i][16])

                if i != len(ele_list):
                    str_line = str_line + '\n'

                elemfileout.write(str_line)

    if Non_DBC_edge_list:
        with open('non_DBC_edge.dat', 'wb') as nDBCout:
            for i in range(0, len(Non_DBC_edge_list)):
                str_line = str(Non_DBC_edge_list[i][0]) + ' ' + str(Non_DBC_edge_list[i][1]) + ' ' + str(Non_DBC_edge_list[i][2]) + \
                       ' ' + str(Non_DBC_edge_list[i][3]) + ' ' + str(Non_DBC_edge_list[i][4]) + ' ' + str(Non_DBC_edge_list[i][5]) + \
                       ' ' + str(Non_DBC_edge_list[i][6]) + ' ' + str(Non_DBC_edge_list[i][7]) + ' ' + str(Non_DBC_edge_list[i][8]) + \
                       ' ' + str(Non_DBC_edge_list[i][9]) + ' ' + str(Non_DBC_edge_list[i][10]) + ' ' + str(Non_DBC_edge_list[i][11]) + '\n'

                nDBCout.write(str_line)

    with open('JacobianEle.dat', 'wb') as Jacout:
        for i in range(0, len(Jac)):
            str_line = str(Jac[i][0]) + ' ' + str(Jac[i][1]) + ' ' + str(Jac[i][2]) + \
                       ' ' + str(Jac[i][3]) + ' ' + str(Jac[i][4]) + ' ' + str(Jac[i][5]) + \
                       ' ' + str(Jac[i][6]) + ' ' + str(Jac[i][7]) + ' ' + str(Jac[i][8]) + '\n'

            Jacout.write(str_line)

    print "Element file handling done"
    #filele = open('elem.bin', 'wb')
    #e_list = np.asarray(ele_list)
    #e_list.tofile(filele)

